<?php

namespace App\Entity\PromoCode;

use App\Repository\PromoCode\PromoCodeRepository;

class PromoCodeFactory implements PromoCodeFactoryInterface
{
    /**
     * @var PromoCodeStrategyInterface
     */
    private $strategy;

    /**
     * @var PromoCodeRepository
     */
    private $promoCodeRepository;

    public function __construct(PromoCodeStrategyInterface $strategy, PromoCodeRepository $promoCodeRepository)
    {
        $this->strategy = $strategy;
        $this->promoCodeRepository = $promoCodeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(int $discount, int $maxUsage): PromoCode
    {
        //TODO add limit of tryouts for create promo code
        $code = $this->strategy->generate();

        $promoCode = new PromoCode();
        $promoCode->setDiscount($this->strategy->generate());
        $promoCode->setMaxUsage($maxUsage);
        $promoCode->setCode($code);
        $promoCode->setStrategyName($this->strategy->getName());

        return $promoCode;
    }
}

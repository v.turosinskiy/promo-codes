<?php

namespace App\Entity\PromoCode;

/**
 * PromoCode Factory.
 */
interface PromoCodeFactoryInterface
{
    /**
     * Create PromoCode by discount and max count usage.
     */
    public function create(int $discount, int $maxUsage): PromoCode;
}

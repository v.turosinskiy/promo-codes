<?php

namespace App\Entity\PromoCode;

class PromoCodeService implements PromoCodeServiceInterface
{
    /**
     * {@inheritdoc}
     */
    public function apply(float $price, PromoCode $promoCode): float
    {
        return round($price * ((100 - $promoCode) / 100), 2);
    }
}

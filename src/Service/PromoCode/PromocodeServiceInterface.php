<?php

namespace App\Entity\PromoCode;

interface PromoCodeServiceInterface
{
    /**
     * Apply promo code.
     */
    public function apply(float $price, PromoCode $promoCode): float;
}

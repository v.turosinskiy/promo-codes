<?php

namespace App\Entity\PromoCode;

class StringPromoCodeStrategy implements PromoCodeStrategyInterface
{
    public const STRATEGY_NAME = 'string';

    /**
     * {@inheritdoc}
     */
    public function generate(): string
    {
        //TODO
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return static::STRATEGY_NAME;
    }
}

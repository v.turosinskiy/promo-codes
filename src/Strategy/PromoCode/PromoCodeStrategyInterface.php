<?php

namespace App\Entity\PromoCode;

interface PromoCodeStrategyInterface
{
    /**
     * Generate code.
     */
    public function generate(): string;

    /**
     * Get strategy name.
     */
    public function getName(): string;
}

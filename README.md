# PromoCode service

## Project info

This Service generate promo codes with different strategies and applying it to order.

## Install

Up docker containers:
```bash
docker compose up -d
```

## Run tests
```bash
vendor/bin/codecept run
```